package com.vehicity.detector;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.BufferUnderflowException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ImagePreviewActivity extends AppCompatActivity {
    final String TAG = "Activity:ImagePreviewActivity";

    private ImageView imgPreviewMain;
    private Bitmap result;
    private String imageUri, ImgType;


    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imgPreviewMain = (ImageView) findViewById(R.id.img_preview_main);
        Intent intent = getIntent();
        imageUri = intent.getStringExtra("imageUri");
        ImgType = intent.getStringExtra("type");
        Log.d(TAG, imageUri);
        Bitmap myBitmap = BitmapFactory.decodeFile(imageUri);
        if (ImgType.equals("ID")) {
            IDcard id = new IDcard(myBitmap);
            imgPreviewMain.setImageBitmap(id.getResult());
            result = id.getResult();
        }else{
            Passport passport = new Passport(myBitmap);
            imgPreviewMain.setImageBitmap(passport.getResult());
            result = passport.getResult();
        }

        try {
            save(result);
        } catch (IOException e) {

        }
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            // TODO Auto-generated method stub
            switch (status){
                case BaseLoaderCallback.SUCCESS:
                    Log.i(TAG, "success to import");
                    break;
                default:
                    super.onManagerConnected(status);
                    Log.i(TAG, "fail to import");
                    break;
            }

        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback);//.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    private void save(Bitmap bmp) throws IOException, BufferUnderflowException {
        OutputStream output = null;
        if(bmp!=null) {
            try {
                output = new FileOutputStream(imageUri);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, output);
                //            output.write(bytes);
            } finally {
                if (null != output) {
                    output.close();
                }
            }
        }
    }
}