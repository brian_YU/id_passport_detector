package com.vehicity.detector;

import android.graphics.Bitmap;
import android.graphics.Matrix;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class Passport {
    private int[] size = {2630,1875}; //Passport width and height (in pixel) new German passport = {2630,1875} old German passport = {2500,1750}
    private int xl = 140;   //Left detection area x coordinate
    private int yl = 1300;  //Left detection area y coordinate
    private int xr = 2750;  //Right detection area x coordinate
    private int yr = 840;   //Right detection area y coordinate
    private int w = 400;    //Detection area width
    private int h = 600;    //Detection area x height
    private int xd = 1000;  //Bottom detection area x coordinate
    private int yd = 2000;  //Bottom detection area y coordinate
    private int wd = 500;   //Bottom detection area width
    private int hd = 300;   //Bottom detection area height

    private int[] start = {0,0,0};

    private Bitmap result;

    public Passport(Bitmap src){
        OpenCVLoader.initDebug();
        Mat srcMat = new Mat();
        Utils.bitmapToMat(src, srcMat);
        detection(src);
        if((start[0] != 0 || start[1] != 0) && start[2] != 0){
            if(start[0] != 0) {
                this.result = Bitmap.createBitmap(src, this.start[0], this.start[2], this.size[0], this.size[1]);
            }
            else if (start[1] != 0){
                this.result = Bitmap.createBitmap(src, this.start[1], this.start[2], this.size[0], this.size[1]);
            }
        }
    }

    private void detection(Bitmap src) {

        Matrix matrix = new Matrix();
        matrix.postRotate(180);

        Mat srcMat = new Mat();
        Mat srcMatCopy = new Mat();
        Mat imageLMat = new Mat();
        Mat imageRMat = new Mat();
        Mat imageDMat = new Mat();
        srcMat.copyTo(srcMatCopy);
        Utils.bitmapToMat(src, srcMat);//convert original bitmap to Mat, R G B.
        srcMat.copyTo(srcMatCopy);

        imageLMat = srcMat.submat(this.yl, this.yl+this.h, this.xl, this.xl+this.w);
        imageRMat = srcMat.submat(this.yr, this.yr+this.h, this.xr, this.xr+this.w);
        imageDMat = srcMat.submat(this.yd, this.yd+this.hd, this.xd, this.xd+this.wd);

        FindEdge(imageLMat, "left");
        FindEdge(imageRMat, "right");
        FindEdge(imageDMat, "down");

    }

    private void FindEdge(Mat srcMat, String part) {
        Mat srcMatCopy = new Mat();
        srcMat.copyTo(srcMatCopy);
        Mat lines = new Mat();
        Mat kernelDilDown = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(7, 3),
                new Point(3, 1));
        Mat kernelDil = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 7),
                new Point(1, 3));
        Mat kernelEroDown = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(11, 1),
                new Point(5, 0));
        Mat kernelEro = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1, 11),
                new Point(0, 5));

        Mat temp = new Mat();
        ArrayList<Mat> dst = new ArrayList<>(3);
        Imgproc.cvtColor(srcMat, temp, Imgproc.COLOR_RGB2HSV);
        srcMat.copyTo(srcMatCopy);

        Core.split(temp, dst);
        /*if (!part.equals("down"))
            dst.get(0).copyTo(srcMat);
        else*/
            dst.get(1).copyTo(srcMat);

        Imgproc.medianBlur(srcMat, srcMat, 21);
        Imgproc.adaptiveThreshold(srcMat, srcMat, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY_INV, 11, 2);
        if (part == "down") {
            Imgproc.erode(srcMat, srcMat, kernelEroDown, new Point(-1, -1), 3);
            Imgproc.dilate(srcMat, srcMat, kernelDilDown, new Point(-1, -1), 3);
        } else {
            Imgproc.erode(srcMat, srcMat, kernelEro, new Point(-1, -1), 5);
            Imgproc.dilate(srcMat, srcMat, kernelDil, new Point(-1, -1), 3);
        }

        Imgproc.Canny(srcMat, srcMat, 30, 300, 5);
        Imgproc.HoughLines(srcMat, lines, 1, Math.PI / 180, 130);//130
        for (int i = 0; i < lines.rows(); i++) {
            double rho = lines.get(i, 0)[0],
                    theta = lines.get(i, 0)[1];
            double a = Math.cos(theta), b = Math.sin(theta);
            double x0 = a * rho, y0 = b * rho;
            Point pt1 = new Point(Math.round(x0 + 1000 * (-b)), Math.round(y0 + 1000 * (a)));
            Point pt2 = new Point(Math.round(x0 - 1000 * (-b)), Math.round(y0 - 1000 * (a)));
            if (part == "left") {
                int x = xl + (int) ((pt1.x + pt2.x) / 2);
                if (x > start[0]) start[0] = x;
            } else if (part == "right") {
                int x = xr + (int) ((pt1.x + pt2.x) / 2) - this.size[0];
                if (x < start[0] || start[1] == 0) start[1] = x;
            } else if (part == "down") {
                int y = yd + (int) ((pt1.y + pt2.y) / 2) - this.size[1];
                if (y < start[2] || start[2] == 0) start[2] = y;
            }
        }
    }

    //If result is null, It means there are no ID card is detected in the image.
    public Bitmap getResult() {
        return result;
    }
}
