# ID card and Passport Detector

## Installation

Detector requires [OpenCV](https://sourceforge.net/projects/opencvlibrary/files/4.5.3/opencv-4.5.3-android-sdk.zip/download) 4.5.3 to run.

### Step 1:
Install the dependencies and devDependencies.
Please refer to [OpenCV in Android Studio](https://www.youtube.com/watch?v=-0Yx1UzozzQ)

### Step 2:
Copy IDcard.java and Passport.java under "pos_crop\app\src\main\java\com\vehicity" to your project.

## Development

#### For ID Card detection:
```
IDcard id = new IDcard(srcImg);
Bitmap result = id.getResult();
```
srcImg is the original image capture from POS.
result is the croped ID card image. (It means no ID card is detected, if the result is empty bitmap.)

#### For Passport detection:
```
Passport id = new Passport(srcImg);
Bitmap result = Passport.getResult();
```
srcImg is the original image capture from POS.
result is the croped ID card image. (It means no Passport is detected, if the result is empty bitmap.)
